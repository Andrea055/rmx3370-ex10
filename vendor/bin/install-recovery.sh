#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:134217728:3026b738b1b94f11dba27b62e4b2e8e20794b787; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:100663296:cf72938850124e9ea8e3c2caca9ab6d5b3dc94cc \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:134217728:3026b738b1b94f11dba27b62e4b2e8e20794b787 && \
      log -t recovery "Installing new oplus recovery image: succeeded" && \
      setprop ro.boot.recovery.updated true || \
      log -t recovery "Installing new oplus recovery image: failed" && \
      setprop ro.boot.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.boot.recovery.updated true
fi
